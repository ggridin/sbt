package lesson4.countmap;

import java.util.Map;

public interface CountMap<T> {
    void add(T t);
    Integer getCount(T t);
    Integer remove(T t);
    int size();
    Map<T, Integer> toMap();
    void toMap(Map<T,Integer> map);
}
