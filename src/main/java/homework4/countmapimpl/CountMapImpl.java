package lesson4.countmapimpl;

import lesson4.countmap.CountMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountMapImpl<T> implements CountMap<T> {

    public class Node {
        private T element;
        private Integer count;

        public Node(T element, Integer count) {
            this.element = element;
            this.count = count;
        }

        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (obj == this) return true;
            if (!(Node.class.isInstance(obj))) return false;
            Node o = (Node) obj;
            return o.element.equals(this.element);
        }
    }

    private ArrayList<Node> data;

    public CountMapImpl() {
        data = new ArrayList<Node>();
    }

    private void setCount(T t, Integer count) {
        for (Node node : data) {
            if ( node.element.equals(t)) node.count = count;
        }
    }

    public void add(T t) {
        if (data.size() == 0 | this.getCount(t) == 0) {
            data.add(new Node(t, 1));
        } else {
            for (Node node : data) {
                if (node.element.equals(t)) {
                    node.count++;
                    break;
                }
            }
        }
    }

    public Integer getCount(T t) {
        for (Node node : data) {
            if (node.element.equals(t)) return node.count;
        }
        return 0;
    }


    public Integer remove(T t) {
        Integer count = getCount(t);
        if (count == 1) data.remove(new Node(t, count));
        if (count > 1) setCount(t, count - 1);
        return count;
    }

    public int size() {
        return data.size();
    }

    public void addAll(CountMapImpl<T> countMapImpl) {
        for (Node node : countMapImpl.data) {
            Integer count = this.getCount(node.element);
            if (count == 0) this.data.add(node);
            else {
                for (Node node2 : this.data) {
                    if (node.element.equals(node2.element)) node2.count += node.count;
                }
            }
        }
    }

    public Map<T, Integer> toMap() {
        HashMap<T, Integer> map = new HashMap<T, Integer>();
        toMap(map);
        return map;
    }

    public void toMap(Map<T,Integer> map) {
        for (Node node : data) map.put(node.element, node.count);
    }
}
