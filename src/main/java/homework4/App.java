package lesson4;

import lesson4.countmapimpl.CountMapImpl;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class App {
    public static void main(String[] args) {

        CountMapImpl<Integer> countMapImpl = new CountMapImpl<Integer>();

        countMapImpl.add(1);
        countMapImpl.add(1);
        countMapImpl.add(2);
        System.out.println(countMapImpl.getCount(1));
        System.out.println(countMapImpl.getCount(2));

        countMapImpl.remove(1);
        System.out.println(countMapImpl.getCount(1));

        countMapImpl.remove(2);
        System.out.println(countMapImpl.getCount(2));

        countMapImpl.addAll(countMapImpl);
        System.out.println(countMapImpl.getCount(1));
        System.out.println(countMapImpl.getCount(2));

        HashMap<Integer, Integer> map = (HashMap<Integer, Integer>) countMapImpl.toMap();
        System.out.println(map);
    }
}
