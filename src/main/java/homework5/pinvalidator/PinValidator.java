package lesson5.pinvalidator;

import lesson5.exceptions.pinvalidatorexception.PinValidatorException;

public class PinValidator {
    public Integer checkPIN(String str) throws PinValidatorException {
        if (str.length() != 4) throw new PinValidatorException("PIN должен состоять из 4 целых чисел");
        try {
            Integer pin = Integer.parseInt(str);
            return pin;
        } catch (NumberFormatException e) {
            throw new PinValidatorException("PIN должен состоять только из чисел", e);
        }
    }
}
