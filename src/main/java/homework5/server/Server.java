package lesson5.server;

import lesson5.exceptions.accountIsLockedException.AccountIsLockedException;
import lesson5.exceptions.serverException.ServerException;

public class Server {
    private Boolean blocked;
    private Long timeOfBlocked;
    private Integer numberOfTry;

    // Эти поля конечно же тут не к месту, но что бы не усложнять
    // проект, нетребующимися по заданию классами, решил написать их тут
    private Double balance;
    private String pin;

    public Server() {
        this.balance = 100.;
        this.pin = null;
        this.blocked = false;
        this.timeOfBlocked = 0L;
        this.numberOfTry = 0;
    }

    public Double balance() {
        return balance;
    }

    public void putMoney(Integer money) throws ServerException {
        if (money % 100 != 0) {
            throw new ServerException("Сумма не кратна 100");
        }
        this.balance += money;
    }

    public void withdrawMoney(Integer money) throws ServerException {
        if (money % 100 != 0) {
            throw new ServerException("Сумма не кратна 100");
        }
        if (money > this.balance) {
            throw new ServerException("На счете недостаточно средств");
        }

        this.balance -= money;
    }

    public void authorization(String pin) throws AccountIsLockedException, ServerException {
        if (blocked) {
            Long currentTime = System.currentTimeMillis();
            Double blockedTime = ((double)(currentTime - timeOfBlocked)) / 1000;
            if (blockedTime < 5.) {
                throw new AccountIsLockedException("Количество попыток авторизации превысило лемит. Повторите попытку через " + (5. - blockedTime) + " секунд");
            } else {
                blocked = false;
                timeOfBlocked = 0L;
                numberOfTry = 0;
            }
        }

        if (numberOfTry == 3) {
            blocked = true;
            timeOfBlocked = System.currentTimeMillis();
            throw new AccountIsLockedException("Количество попыток авторизации превысило лемит. Повторите попытку через 5 секунд");
        }

        if (pin.equals("1234")) {

        } else {
            numberOfTry++;
            throw new ServerException("Ошибка авторизации. Введен неправильный PIN");
        }
    }
}
