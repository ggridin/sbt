package lesson5.teminalimpl;

import lesson5.exceptions.accountIsLockedException.AccountIsLockedException;
import lesson5.exceptions.pinvalidatorexception.PinValidatorException;
import lesson5.exceptions.serverException.ServerException;
import lesson5.pinvalidator.PinValidator;
import lesson5.server.Server;
import lesson5.terminal.Terminal;
import lesson5.userInterfaceImpl.UserInterfaceImpl;
import lesson5.userinterface.UserInterface;

public class TerminalImpl implements Terminal {
    private static Server server;
    private static PinValidator pinValidator;
    private static UserInterface userInterface;

    public TerminalImpl() {
        server = new Server();
        pinValidator = new PinValidator();
        userInterface = new UserInterfaceImpl();
    }

    public void run(){
        userInterface.message("Здравствуйте.");
        while (true) {
            try {
                userInterface.message("Введите PIN");
                String pin = userInterface.message();
                pinValidator.checkPIN(pin);
                server.authorization(pin);
                userInterface.message("Введите 1 чтобы узнать баланс, 2 - получить наличные, 3 - внести деньги на счет");
                String command = userInterface.message();
                if (command.equals("1")) {
                    userInterface.message(server.balance().toString());
                }
                else if (command.equals("2")) {
                    userInterface.message("Введите сумму");
                    server.withdrawMoney(Integer.valueOf(userInterface.message()));
                }
                else if (command.equals("3")) {
                    userInterface.message("Введите сумму");
                    server.putMoney(Integer.valueOf(userInterface.message()));
                }
            }
            //Следующие 3 исключения можно было бы заменить одним, т.к. приложение реагирует на них одинаково,
            //однако в общем случае это не так.
            catch (PinValidatorException e) {
                userInterface.message(e.getMessage());
            }
            catch (ServerException e ) {
                userInterface.message(e.getMessage());
            }
            catch (AccountIsLockedException e) {
                userInterface.message(e.getMessage());
            }
            catch (Exception e) {
                userInterface.message("Непредвиденная ошибка");
            }
        }
    }
}
