package lesson5.exceptions.pinvalidatorexception;

public class PinValidatorException extends Exception{
    public PinValidatorException(String msg) {
        super(msg);
    }
    public PinValidatorException(String msg, Throwable cause) { super(msg,cause);}
}
