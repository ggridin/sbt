package lesson5.exceptions.accountIsLockedException;

public class AccountIsLockedException extends Exception {
    public AccountIsLockedException(String msg, Throwable cause) {
        super(msg,cause);
    }
    public AccountIsLockedException(String msg) {
        super(msg);
    }
}
