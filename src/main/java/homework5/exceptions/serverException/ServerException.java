package lesson5.exceptions.serverException;

public class ServerException extends Exception {
    public ServerException(String msg) {
        super(msg);
    }
}
