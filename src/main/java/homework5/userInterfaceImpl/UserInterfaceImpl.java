package lesson5.userInterfaceImpl;

import lesson5.userinterface.UserInterface;

import java.util.Scanner;

public class UserInterfaceImpl implements UserInterface {

    private Scanner scanner;

    public UserInterfaceImpl() {
        this.scanner = new Scanner(System.in);
    }

    public void message(String message) {
        System.out.println(message);
    }

    public String message() {
        return scanner.nextLine();
    }
}
