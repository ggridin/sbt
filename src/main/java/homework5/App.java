package lesson5;

import lesson5.teminalimpl.TerminalImpl;
import lesson5.terminal.Terminal;

public class App {
    public static  void main(String[] args) {
        TerminalImpl terminal = new TerminalImpl();
        terminal.run();
    }
}
