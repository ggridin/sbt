import validator.Validator;
import person.Person;
import validatorimpl.ValidatorImpl;

public class App {
    public static void main(String[] str) {
        try {
            Person person = new Person("1111", "11111", "234234", null, 1, "1111231231");
            Validator<Person> validator = new ValidatorImpl<Person>();
            validator.validate(person);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
