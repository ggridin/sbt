package validatorimpl;

import validator.Validator;
import annotations.max.Max;
import annotations.maxlenght.MaxLenght;
import annotations.min.Min;
import annotations.minlenght.MinLenght;
import annotations.notempty.NotEmpty;
import annotations.notnull.NotNull;

import java.lang.reflect.Field;
import java.util.Date;

public class ValidatorImpl<T> implements Validator<T> {

    public void validate(T obj) throws IllegalArgumentException, IllegalAccessException {
            Class<?> clazz = obj.getClass();
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Max.class)) {
                    Max annotation = field.getAnnotation(Max.class);
                    int max = annotation.value();

                    int value = (Integer) field.get(obj);
                    if (value > max)
                        throw new IllegalArgumentException(field.getName() + " > " + max);
                }
                if (field.isAnnotationPresent(Min.class)) {
                    Min annotation = field.getAnnotation(Min.class);
                    int min = annotation.value();

                    int value = (Integer) field.get(obj);
                    if (value < min)
                        throw new IllegalArgumentException(field.getName() + " < " + min);
                }
                if (field.isAnnotationPresent(MaxLenght.class)) {
                    MaxLenght annotation = field.getAnnotation(MaxLenght.class);
                    int maxLenght = annotation.value();

                    String value = (String) field.get(obj);
                    if (value.length() > maxLenght)
                        throw new IllegalArgumentException("lenght of " + field.getName() +  " > " + maxLenght);
                }
                if (field.isAnnotationPresent(MinLenght.class)) {
                    MinLenght annotation = field.getAnnotation(MinLenght.class);
                    int minLenght = annotation.value();

                    String value = (String) field.get(obj);
                    if (value.length() < minLenght)
                        throw new IllegalArgumentException("lenght of " + field.getName()  +  " < " + minLenght);
                }
                if (field.isAnnotationPresent(NotEmpty.class)) {
                    NotEmpty annotation = field.getAnnotation(NotEmpty.class);

                    String value = (String) field.get(obj);
                    if (value.length() == 0)
                        throw new IllegalArgumentException(field.getName() + " is empty");
                }
                if (field.isAnnotationPresent(NotNull.class)) {
                    NotNull annotation = field.getAnnotation(NotNull.class);

                    Date value = (Date) field.get(obj);
                    if (value == null)
                        throw new IllegalArgumentException(field.getName() + " is null");
                }
            }
    }
}
