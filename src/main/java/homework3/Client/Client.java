package homework3.Client;

import homework3.Credit.Credit;

import java.util.ArrayList;

public class Client {
    private Integer id;
    private String name;

    private ArrayList<Credit> credits;

    public Client(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.credits = new ArrayList<Credit>();
    }

    public ArrayList<Credit> getCredits() {
        return credits;
    }

    public String getName() {
        return name;
    }

    public Boolean addCredit(Credit credit) {
        boolean b = false;
        for (Credit crdt: credits) {
            b |= crdt.equals(credit);
        }

        if (!b) credits.add(credit);
        return b;
    }

    @Override
    public int hashCode() {
        return 31 * id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof  Client)) return false;

        Client client = (Client) obj;

        return id.equals(client.id) && name.equals(client.name);
    }

    @Override
    public String toString() {
        return "{" +
                    "\"id\": " + id + "," +
                    "\"name\": " + name + "," +
                    "\"credits\": " + credits +
                "}";
    }

}
