package homework3.Service;

import homework3.Client.Client;
import homework3.Credit.Credit;

import java.util.ArrayList;

public class Service {
    public static Double getSum(Client client) {
        Double sum = 0.;
        for (Credit credit : client.getCredits()) sum += credit.getSum();
        return sum;
    }

    public static void sortByName(ArrayList<Client> clients) {
        clients.sort((Client c1, Client c2) -> c1.getName().compareTo(c2.getName()));
    }

    public static void sortBySum(ArrayList<Client> clients) {
        clients.sort((Client c1, Client c2) -> Service.getSum(c1).compareTo(Service.getSum(c2)));
    }
}
