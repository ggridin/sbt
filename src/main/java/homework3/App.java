package homework3;

import homework3.Client.Client;
import homework3.Credit.Credit;
import homework3.Service.Service;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        Credit credit1 = new Credit(1,1.);
        Credit credit2 = new Credit(2,2.);
        Credit credit3 = new Credit(3,3.);
        Credit credit4 = new Credit(4,4.);

        Client client1 = new Client(1, "Anton");
        Client client2 = new Client(2, "Alex");

        client1.addCredit(credit1);
        client1.addCredit(credit2);
        client2.addCredit(credit3);
        client2.addCredit(credit4);

        ArrayList<Client> clients = new ArrayList<Client>();
        clients.add(client1);
        clients.add(client2);

        Service.sortByName(clients);
        System.out.println(clients);
    }
}
