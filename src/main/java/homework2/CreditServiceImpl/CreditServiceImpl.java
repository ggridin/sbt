package homework2.CreditServiceImpl;

import homework2.Client.Client;
import homework2.Credit.Credit;
import homework2.CreditService.CreditService;

import java.util.ArrayList;

public class CreditServiceImpl implements CreditService {
    @Override
    public ArrayList<Credit> getCredits(Client client) {
        return null;
    }

    @Override
    public void repayLoan(Client client) {
        for (Credit crdt : this.getCredits(client)) {
            if (crdt.getLoanBalance() <= 0) {
                crdt.isActive(false);
                System.out.println("Кредит: " + crdt + "\n" +
                                   "Клиента: " + client + "\n" +
                                   "Погашен");
            }
        }
    }
}
