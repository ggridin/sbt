package homework2.CreditService;

import homework2.Client.Client;
import homework2.Credit.Credit;

import java.util.ArrayList;

public interface CreditService {
    ArrayList<Credit> getCredits(Client client);
    void repayLoan(Client client);
}
