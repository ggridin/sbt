package homework2;

import homework2.Client.Client;
import homework2.Credit.Credit;
import homework2.CreditService.CreditService;
import homework2.CreditServiceImpl.CreditServiceImpl;
import homework2.Transfer.Transfer;

public class App {
    public  static  void main(String[] args) {

        Client client = new Client(0, "name");
        Credit credit1 = new Credit(0, 1.);
        Credit credit2 = new Credit(2, 3.);
        Credit credit3 = new Credit(3, 4.);

        client.addCredit(credit1);
        client.addCredit(credit2);
        client.addCredit(credit3);

        CreditService creditService = new CreditServiceImpl();

        Transfer.transfer(credit1, 1.);
        creditService.repayLoan(client);
    }
}