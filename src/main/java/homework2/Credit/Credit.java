package homework2.Credit;

public class Credit {
    private Integer id;
    private Double sum;
    private Double loanBalance;
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public Double getSum() {
        return sum;
    }

    public Double getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(Double loanBalance) {
        this.loanBalance = loanBalance;
    }

    public Boolean isActive(){
        return isActive;
    }

    public void isActive(Boolean b){
        this.isActive = b;
    }

    public Credit(Integer id, Double sum) {
        this.id = id;
        this.sum = sum;
        this.loanBalance = sum;
        this.isActive = true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Credit)) return false;

        Credit credit = (Credit) obj;

        return id.equals(credit.id) && sum.equals(credit.sum);
    }

    @Override
    public String toString() {
        return "{" +
                    "\"id\": " + id + "," +
                    "\"sum\": " + sum + "," +
                    "\"locanBalance\": " + loanBalance + "," +
                    "\"isActive\": " + isActive  +
                "}";
    }
}
