package homework2.Transfer;

import homework2.Credit.Credit;

public class Transfer {
    public static Boolean transfer(Credit credit, Double value) {
        if (credit.isActive()) credit.setLoanBalance(credit.getLoanBalance() - value);
        else return false;
        return true;
    }
}
